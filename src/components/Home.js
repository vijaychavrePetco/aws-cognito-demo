import React, { Fragment } from 'react';
import Hero from './Hero';
import HomeContent from './HomeContent';

export default function Home() {
  return (
    <Fragment>
      <div style={{margin: 'auto', textAlign: 'center', marginTop: '10px', background: 'blue', height: '80vh'}}>
        Home Page
      </div>
    </Fragment>
  )
}
